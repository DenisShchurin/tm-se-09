package ru.shchurin.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleUtil {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static String getStringFromConsole() throws IOException {
        return reader.readLine();
    }
}

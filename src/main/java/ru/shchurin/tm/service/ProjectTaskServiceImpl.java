package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ProjectTaskService;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.api.ProjectRepository;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.exception.*;
import java.util.ArrayList;
import java.util.List;

public final class ProjectTaskServiceImpl implements ProjectTaskService {
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;

    public ProjectTaskServiceImpl(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> getTasksOfProject(@Nullable final String userId, @Nullable final String name) throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Task> projectTasks = new ArrayList<>();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        @Nullable String projectId = null;
        for (Project project : projects) {
            if (project.getName().equals(name))
                projectId = project.getId();
        }

        @NotNull final List<Task> allTasks = taskRepository.findAll(userId);
        for (Task task : allTasks) {
            if (task.getProjectId().equals(projectId))
                projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeProjectAndTasksByName(@Nullable final String userId, @Nullable final String name) throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        String projectId = null;
        for (Project project : projects) {
            if (project.getName().equals(name))
                projectId = project.getId();
        }
        @NotNull final List<Task> tasks = taskRepository.findAll(userId);
        for (Task task : tasks) {
            if (task.getProjectId().equals(projectId))
                taskRepository.remove(userId, task.getId());
        }
        projectRepository.removeByName(userId, name);
    }
}

package ru.shchurin.tm.exception;

public class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("DESCRIPTION MUST NOT BE NULL OR EMPTY");
    }
}

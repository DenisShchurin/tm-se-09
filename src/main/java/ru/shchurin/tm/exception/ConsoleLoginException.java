package ru.shchurin.tm.exception;

public class ConsoleLoginException extends Exception {
    public ConsoleLoginException() {
        super("YOU ENTERED INCORRECT LOGIN");
    }
}

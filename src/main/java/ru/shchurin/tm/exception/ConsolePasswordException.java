package ru.shchurin.tm.exception;

public class ConsolePasswordException extends Exception {
    public ConsolePasswordException() {
        super("YOU ENTERED INCORRECT NEW PASSWORD");
    }
}

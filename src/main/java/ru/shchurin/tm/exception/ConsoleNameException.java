package ru.shchurin.tm.exception;

public class ConsoleNameException extends Exception {
    public ConsoleNameException() {
        super("YOU ENTERED INCORRECT NAME");
    }
}

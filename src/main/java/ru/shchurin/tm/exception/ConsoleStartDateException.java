package ru.shchurin.tm.exception;

public class ConsoleStartDateException extends Exception {
    public ConsoleStartDateException() {
        super("YOU ENTERED INCORRECT START_DATE");
    }
}

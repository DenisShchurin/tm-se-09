package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.*;

import java.util.*;

public final class TaskRepositoryImpl implements TaskRepository {
    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull
        final List<Task> userTasks = new ArrayList<>();
        for (Task task : tasks.values()) {
            if (userId.equals(task.getUserId()))
                userTasks.add(task);
        }
        return userTasks;
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) {
        @NotNull
        final Task task = tasks.get(id);
        if (task.getUserId().equals(userId)) {
            return task;
        } else {
            return null;
        }
    }

    @Override
    public void persist(@NotNull final Task task) throws AlreadyExistsException {
        if (tasks.containsKey(task.getId()))
            throw new AlreadyExistsException();
        tasks.put(task.getId(), task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        @NotNull
        final Task task = tasks.get(id);
        if (task.getUserId().equals(userId))
            tasks.remove(id);
    }

    @Override
    public void removeAll(final String userId) {
        for (Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> e = it.next();
            if (userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }

    @Override
    public void removeByName(final String userId, final String name) {
        for (Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> e = it.next();
            if (name.equals(e.getValue().getName()) && userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }
}

package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.api.UserRepository;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class UserRepositoryImpl implements UserRepository {
    @NotNull
    private final Map<String, User> users = new HashMap<>();

    @Override
    public Collection<User> findAll() {
        return users.values();
    }

    @Override
    public User findOne(@NotNull final String id) {
        return users.get(id);
    }

    @Override
    public void persist(@NotNull final User user) throws AlreadyExistsException {
        if (users.containsKey(user.getId())) {
            throw new AlreadyExistsException();
        }
        users.put(user.getId(), user);
    }

    @Override
    public void merge(@NotNull final User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void remove(@NotNull final String id) {
        users.remove(id);
    }

    @Override
    public void removeAll() {
        users.clear();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        for (Iterator<Map.Entry<String, User>> it = users.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, User> e = it.next();
            if (login.equals(e.getValue().getLogin())) {
                it.remove();
            }
        }
    }

    @Override
    public boolean updatePassword(@NotNull final String login, @NotNull final String hashPassword, @NotNull final String newHashPassword) {
        for (User user : users.values()) {
            if (user.getLogin().equals(login) && user.getHashPassword().equals(hashPassword)) {
                user.setHashPassword(newHashPassword);
                return true;
            }
        }
        return false;
    }
}

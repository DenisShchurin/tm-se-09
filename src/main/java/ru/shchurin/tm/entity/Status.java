package ru.shchurin.tm.entity;

public enum Status {
    STATUS_SCHEDULED,
    STATUS_IN_PROGRESS,
    STATUS_DONE;

    public void displayName(){
        for (Status status : Status.values()) {
            System.out.println(status);
        }
    }
}

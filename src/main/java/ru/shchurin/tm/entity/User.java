package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class User {

    @Nullable
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String login;

    @Nullable
    private String hashPassword;

    @Nullable
    private Role role;

    public User(@Nullable String login, @Nullable String hashPassword) {
        this.login = login;
        this.hashPassword = hashPassword;
        this.role = Role.ROLE_USER;
    }

    public User(@Nullable String login, @Nullable String hashPassword, @Nullable Role role) {
        this.login = login;
        this.hashPassword = hashPassword;
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", hashPassword='" + hashPassword + '\'' +
                ", role=" + role +
                '}';
    }
}

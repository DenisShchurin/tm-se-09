package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskClearCommand extends AbstractCommand {
    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all task.";
    }

    @Override
    public void execute () throws Exception {
        serviceLocator.getTaskService().removeAll(serviceLocator.getCurrentUser().getId());
        System.out.println("[ALL TASK REMOVED]");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

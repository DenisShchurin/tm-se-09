package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;
import ru.shchurin.tm.exception.ConsoleHashPasswordException;
import ru.shchurin.tm.exception.ConsoleLoginException;
import ru.shchurin.tm.util.ConsoleUtil;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public final class UserRegistrationCommand extends AbstractCommand {
    private final boolean safe = true;

    @NotNull
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Register user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull
        final String login = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER PASSWORD:");
        @NotNull
        final String password = ConsoleUtil.getStringFromConsole();
        @NotNull
        final String hashOfPassword = serviceLocator.getUserService().getHashOfPassword(password);
        @NotNull
        final User user = new User(login, hashOfPassword);
        try {
            serviceLocator.getUserService().persist(user);
        } catch (AlreadyExistsException | ConsoleLoginException | ConsoleHashPasswordException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[USER REGISTERED]");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.List;

public final class ExitCommand extends AbstractCommand {
    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "exit the application";
    }

    @Override
    public void execute() {
        System.out.println("GOOD BYE");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

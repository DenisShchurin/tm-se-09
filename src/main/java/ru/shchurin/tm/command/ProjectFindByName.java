package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class ProjectFindByName extends AbstractCommand {
    private final boolean safe = false;
    @NotNull
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "find-project-by-name";
    }

    @Override
    public String getDescription() {
        return "Find project by name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull
        final String name = ConsoleUtil.getStringFromConsole();
        @NotNull
        Map<Project> projects = serviceLocator.getProjectService().findAll()
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskFindByDescription extends AbstractCommand{
    private final boolean safe = false;

    @NotNull
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "find-task-by-description";
    }

    @Override
    public String getDescription() {
        return "Find task by description.";
    }

    @Override
    public void execute() throws Exception {

    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

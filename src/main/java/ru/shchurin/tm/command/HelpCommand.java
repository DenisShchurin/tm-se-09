package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {
    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println(command.getCommand() + ": " + command.getDescription());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

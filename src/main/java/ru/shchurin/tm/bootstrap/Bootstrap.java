package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shchurin.tm.api.*;
import ru.shchurin.tm.command.*;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.repository.*;
import ru.shchurin.tm.service.*;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);

    @NotNull
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    @Nullable
    private User currentUser;

    @Nullable
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.shchurin.tm").getSubTypesOf(AbstractCommand.class);

    private void register(@Nullable final Class clazz) throws Exception {
        if (clazz == null) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();

        @Nullable final String cliCommand = command.getCommand();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void init() throws Exception {
        if (classes == null) return;
        for (@Nullable final Class clazz : classes)
            register(clazz);
    }

    public void initUsers() throws ConsoleLoginException, ConsoleHashPasswordException, AlreadyExistsException {
        userService.persist(new User("Admin", "e3afed0047b08059d0fada10f400c1e5", Role.ROLE_ADMIN));
        userService.persist(new User("User", "8f9bfe9d1345237cb3b2b205864da075", Role.ROLE_USER));
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        if (!abstractCommand.isSafe() && currentUser == null)
            throw new UserNotAuthorized();
        if (!abstractCommand.isSafe() && currentUser != null && !abstractCommand.getRoles().contains(currentUser.getRole()))
            throw new UserHasNotAccess();
        abstractCommand.execute();
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public UserService getUserService() {
        return userService;
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable User currentUser) {
        this.currentUser = currentUser;
    }
}

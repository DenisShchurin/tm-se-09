package ru.shchurin.tm.comparator;

import ru.shchurin.tm.entity.Project;

import java.util.Comparator;

public class ProjectStartDateComparator implements Comparator<Project> {
    @Override
    public int compare(Project o1, Project o2) {
        return o1.getStartDate().compareTo(o2.getStartDate());
    }
}

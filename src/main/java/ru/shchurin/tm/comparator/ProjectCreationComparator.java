package ru.shchurin.tm.comparator;

import ru.shchurin.tm.entity.Project;

import java.util.Comparator;

public class ProjectCreationComparator implements Comparator<Project> {
    @Override
    public int compare(Project o1, Project o2) {
        return o1.getCreationDate().compareTo(o2.getCreationDate());
    }
}

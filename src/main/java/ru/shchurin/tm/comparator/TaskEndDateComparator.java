package ru.shchurin.tm.comparator;

import ru.shchurin.tm.entity.Task;

import java.util.Comparator;

public class TaskEndDateComparator implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        return o1.getEndDate().compareTo(o2.getEndDate());
    }
}

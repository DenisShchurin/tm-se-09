package ru.shchurin.tm.comparator;

import ru.shchurin.tm.entity.Task;

import java.util.Comparator;

public class TaskCreationComparator implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        return o1.getCreationDate().compareTo(o2.getCreationDate());
    }
}

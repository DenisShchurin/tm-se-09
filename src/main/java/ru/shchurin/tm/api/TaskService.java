package ru.shchurin.tm.api;

import ru.shchurin.tm.entity.Task;

import java.util.List;

public interface TaskService {
    List<Task> findAll(String userId) throws Exception;

    Task findOne(String userId, String id) throws Exception;

    void persist(Task task) throws Exception;

    void merge(Task task) throws Exception;

    void remove(String userId, String id) throws Exception;

    void removeAll(String userId) throws Exception;

    void removeByName(String userId, String name) throws Exception;
}

package ru.shchurin.tm;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.*;

public final class Application {
    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.initUsers();
        bootstrap.start();
    }
}
